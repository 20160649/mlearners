# you add libraries here too 
# make sure that the methods you write here have required libraries

import pandas as pd
import numpy as np


class MAlg(object):
	"""
	This class contains various machine learning algorithms
	as methods to predict class over iris dataset
	
	"""
	# this is like a constructor
	def __init__(self):
		pass




	#############################################################
	######## Specific function for X algorithm ##################
	#############################################################
	def Xmethod(self,trainX, trainY, testX, testY):

		results = ['This will contain accuracy and other results']
		return results
	
	



	# this is like a de-constructor
	def __del__(self):
		pass
		